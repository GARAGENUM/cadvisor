# CADVISOR

Service pour monitorer les conteneurs via prometheus

## PRE REQUIS :paperclip:

- Docker + compose :whale:
- DNS pointant vers Cadvisor host

## CONFIG :wrench:

### CADVISOR HOST

- Créer cadvisor network:
```bash
sudo docker network create --driver=bridge --subnet=10.10.10.0/24 --gateway=10.10.10.1 cadvisor
```

- Créer les credentials:
```bash
sudo apt-get install apache2-utils
sudo htpasswd -c -b /etc/nginx/.htpasswd <user> <mot-de-passe>
```

- Nginx reverse proxy config:
```conf
server {
    listen 80;
    server_name cadvisor.domaine.tld;

    if ($host = cadvisor.odoo.legaragenumerique.xyz) {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl;
    server_name cadvisor.domaine.tld;

    location / {
        auth_basic             "Restricted";
        auth_basic_user_file   .htpasswd;

        proxy_pass             http://10.10.10.100:8080;
        proxy_read_timeout     900;
    }

    access_log /var/log/nginx/cadvisor.access.log;
    error_log /var/log/nginx/cadvisor.error.log;

    ssl_certificate /etc/letsencrypt/live/cadvisor.domaine.tld/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/cadvisor.domaine.tld/privkey.pem;
}
```

### PROMETHEUS HOST

- Prometheus config:
```yml
# CADVISOR SCRAPING ENDPOINT
  - job_name: 'odoo-docker'
    scheme: https
    basic_auth:
      username: '<user>'
      password: '<mot-de-passe>'
    tls_config:
      insecure_skip_verify: true
    scrape_interval: 5s
    honor_labels: true
    static_configs:
      - targets: ['cadvisor.domaine.tld']
        labels:
          instance: odoo-docker
```

### GARAFANA DASHBOARD :bar_chart:

- Importer une dashboard via ID: 13946

## UTILISATION :rocket:

```bash
docker compose up -d
```